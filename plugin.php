<?php
/*
Plugin Name: Ultimate Member - Contact Form
Plugin URI: https://plusplugins.com/
Description: Contact form for Ultimate Member.
Author: PlusPlugins
Version: 1.1.1
Author URI: https://plusplugins.com
Text Domain: pp-contact
 */

define( 'PP_CONTACT_ITEM_NAME', 'Ultimate Member Contact Form' );
define( 'PP_CONTACT_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'PP_CONTACT_PLUGIN_URI', plugin_dir_url( __FILE__ ) );
define( 'PP_CONTACT_REQUIRES', '1.3.28' );
define( 'PP_CONTACT_STORE_URL', 'https://plusplugins.com' );
define( 'PP_CONTACT_VERSION', '1.1.1' );

require PP_CONTACT_PLUGIN_DIR . 'init.php';

if ( ! class_exists( 'EDD_SL_Plugin_Updater' ) ) {
	include PP_CONTACT_PLUGIN_DIR . 'EDD_SL_Plugin_Updater.php';
}

function pp_contact_plugin_updater() {
	if ( ! function_exists( 'um_get_option' ) ) {
		return;
	}

	$item_key    = 'pp_contact_license_key';
	$item_status = 'pp_contact_license_status';
	$license_key = trim( um_get_option( $item_key ) );
	$edd_updater = new EDD_SL_Plugin_Updater( PP_CONTACT_STORE_URL, __FILE__, array(
			'version'   => PP_CONTACT_VERSION,
			'license'   => $license_key,
			'item_name' => PP_CONTACT_ITEM_NAME,
			'author'    => 'PlusPlugins',
			'url'       => home_url(),
		)
	);
}

add_action( 'admin_init', 'pp_contact_plugin_updater', 0 );

add_filter( 'um_licensed_products_settings', 'pp_contact_license_key' );
function pp_contact_license_key( $array ) {
	if ( ! function_exists( 'um_get_option' ) ) {
		return;
	}

	$item_key    = 'pp_contact_license_key';
	$item_status = 'pp_contact_license_status';

	$array[] = array(
		'id'       => $item_key,
		'type'     => 'text',
		'title'    => 'Contact Form License Key',
		'compiler' => true,
	);

	return $array;
}

add_filter( 'redux/options/um_options/compiler', 'pp_contact_license_status', 10, 3 );
function pp_contact_license_status( $options, $css, $changed_values ) {
	if ( ! function_exists( 'um_get_option' ) ) {
		return;
	}

	$item_key    = 'pp_contact_license_key';
	$item_status = 'pp_contact_license_status';

	if ( isset( $options[ $item_key ] ) && isset( $changed_values[ $item_key ] ) && $options[ $item_key ] != $changed_values[ $item_key ] ) {
		if ( $options[ $item_key ] == '' ) {
			$license    = trim( $options[ $item_key ] );
			$api_params = array(
				'edd_action' => 'deactivate_license',
				'license'    => $changed_values[ $item_key ],
				'item_name'  => urlencode( PP_CONTACT_ITEM_NAME ), // the name of our product in EDD
				'url'        => home_url(),
			);

			$response = wp_remote_get( add_query_arg( $api_params, PP_CONTACT_STORE_URL ), array(
				'timeout'   => 30,
				'sslverify' => false
			) );
			
			if ( is_wp_error( $response ) ) {
				return false;
			}

			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
			delete_option( $item_status );
		} else {
			$license    = trim( $options[ $item_key ] );
			$api_params = array(
				'edd_action' => 'activate_license',
				'license'    => $license,
				'item_name'  => urlencode( PP_CONTACT_ITEM_NAME ), // the name of our product in EDD
				'url'        => home_url(),
			);

			$response = wp_remote_get( add_query_arg( $api_params, PP_CONTACT_STORE_URL ), array(
				'timeout'   => 30,
				'sslverify' => false
			) );
			if ( is_wp_error( $response ) ) {
				return false;
			}

			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
			update_option( $item_status, $license_data->license );
		}
	}
}

add_action( 'plugins_loaded', function () {
	load_plugin_textdomain( 'pp-contact', false, PP_CONTACT_PLUGIN_DIR . 'lang' );
} );
