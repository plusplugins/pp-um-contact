<?php

class PP_Contact_Settings {

	function __construct() {

		require_once PP_CONTACT_PLUGIN_DIR . 'includes/cmb2/init.php';

		add_action('cmb2_admin_init', array($this, 'directory_metabox'));

		add_filter('redux/options/um_options/sections', array($this, 'add_contact_tab'), 9005);

		add_action('um_after_account_privacy', array($this, 'account_privacy'), 15);

	}

	function directory_metabox() {

		$cmb = new_cmb2_box(array(
			'id'           => 'pp_contact_directory_metabox',
			'title'        => __('Contact Form', 'pp-contact'),
			'object_types' => array('um_directory'),
			'context'      => 'normal',
			'priority'     => 'default',
			'show_names'   => true,
		));

		$cmb->add_field(array(
			'name' => __('Show contact form button', 'pp-contact'),
			'id'   => '_um_pp_contact_form_button',
			'type' => 'checkbox',
		));
	}

	function add_contact_tab($sections) {

		global $ultimatemember;

		$roles = $ultimatemember->query->get_roles();

		$fields = array();

		$fields[] = array(

			'id'      => 'pp_contact_have_roles',
			'type'    => 'select',
			'title'   => __('Roles with contact form', 'pp-contact'),
			'default' => array(),
			'desc'    => __('Select roles that can have contact form. Leave blank for all roles.', 'pp-contact'),
			'multi'   => true,
			'options' => $roles,

		);

		$fields[] = array(

			'id'      => 'pp_contact_view_roles',
			'type'    => 'select',
			'title'   => __('Roles that can use contact form', 'pp-contact'),
			'default' => array(),
			'desc'    => __('Select roles that can see and use the contact form. Leave blank for all roles and visitors.', 'pp-contact'),
			'multi'   => true,
			'options' => $roles + array('visitor' => 'Visitor'),

		);

		$fields[] = array(

			'id'      => 'pp_contact_recaptcha',
			'type'    => 'switch',
			'title'   => __('Enable reCAPTCHA', 'pp-contact'),
			'default' => 0,
			'desc'    => __('Show reCAPTCHA on contact form. Requires the free <a href="https://ultimatemember.com/extensions/google-recaptcha/" target="_blank">UM reCAPTCHA</a> plugin to be installed and configured.', 'pp-contact'),
			'on'      => __('Yes', 'pp-contact'),
			'off'     => __('No', 'pp-contact'),
		);

		$fields[] = array(
			'id'     => 'pp_contact_mail_section_start',
			'type'   => 'section',
			'title'  => __('Email options', 'pp-contact'),
			'indent' => true,
		);

		$fields[] = array(

			'id'      => 'pp_contact_mail_on',
			'type'    => 'switch',
			'title'   => __('Enable email', 'pp-contact'),
			'default' => 1,
			'desc'    => __('Under normal circumstances this option should remain active.', 'pp-contact'),
			'on'      => __('Yes', 'pp-contact'),
			'off'     => __('No', 'pp-contact'),

		);

		$fields[] = array(

			'id'      => 'pp_contact_mail_sub',
			'type'    => 'text',
			'title'   => __('Email subject', 'pp-contact'),
			'default' => 'New contact form submission from {sitename}!',
		);

		$fields[] = array(

			'id'      => 'pp_contact_mail',
			'type'    => 'textarea',
			'title'   => __('Email body', 'pp-contact'),
			'rows'    => 12,
			'default' => 'Hi {recipient_name},' . "\r\n\r\n" .
			'You\'ve received a new message from {sender_name} ({sender_email}).' . "\r\n\r\n" .
			'Message:' . "\r\n\r\n" .
			'{message}' . "\r\n\r\n" .
			'Do not respond to this email directly. If you wish to reply to this message, send an email to {sender_email}.' . "\r\n" .
			'If you do not wish to be contacted on the site, you can disable your contact form in your account settings.' . "\r\n\r\n" .
			'Regards,' . "\r\n" .
			'{site_name}',
		);

		$fields[] = array(
			'id'    => 'pp_contact_tags_message',
			'type'  => 'info',
			'style' => 'success',
			'title' => __('The following tags may be used:', 'pp-contact'),
			'icon'  => 'el-icon-info-sign',
			'desc'  => __('{sitename}, {recipient_name}, {sender_name}, {sender_email}, {message}', 'pp-contact'),
		);

		$fields[] = array(
			'id'     => 'pp_contact_mail_section_end',
			'type'   => 'section',
			'indent' => false,
		);

		$sections[] = array(

			'icon'       => 'um-faicon-envelope',
			'title'      => __('Contact Form', 'pp-contact'),
			'fields'     => $fields,
			'subsection' => false,

		);

		return $sections;

	}

	function account_privacy() {

		$user_role = get_user_meta(get_current_user_id(), 'role', true);

		$have_roles = um_get_option('pp_contact_have_roles');

		if (isset($have_roles) && !in_array($user_role, $have_roles)) {
			//		return false;
		}

		$hide = get_user_meta(get_current_user_id(), 'pp_hide_contact_form', true);

		?>

		<div class="um-field um-field-pp_hide_contact_form um-field-radio" data-key="pp_hide_contact_form">
			<div class="um-field-label">
				<label for="pp_hide_contact_form">Disable the contact form for my profile</label>
				<span class="um-tip um-tip-w" original-title="Here you can disable the contact form that allows users and visitors to contact you via email">
					<i class="um-icon-help-circled"></i>
				</span>
				<div class="um-clear"></div>
			</div>
			<div class="um-field-area">

				<?php if ($hide != 'Yes') {

			?>

					<label class="um-field-radio um-field-half active">
						<input type="radio" name="pp_hide_contact_form" value="No" checked="">
						<span class="um-field-radio-state"><i class="um-icon-android-radio-button-on"></i></span>
						<span class="um-field-radio-option">No</span>
					</label>
					<label class="um-field-radio um-field-half right">
						<input type="radio" name="pp_hide_contact_form" value="Yes">
						<span class="um-field-radio-state"><i class="um-icon-android-radio-button-off"></i></span>
						<span class="um-field-radio-option">Yes</span>
					</label>

				<?php

		} else {

			?>

					<label class="um-field-radio um-field-half">
						<input type="radio" name="pp_hide_contact_form" value="No">
						<span class="um-field-radio-state"><i class="um-icon-android-radio-button-off"></i></span>
						<span class="um-field-radio-option">No</span>
					</label>
					<label class="um-field-radio um-field-half right active">
						<input type="radio" name="pp_hide_contact_form" value="Yes" checked="">
						<span class="um-field-radio-state"><i class="um-icon-android-radio-button-on"></i></span>
						<span class="um-field-radio-option">Yes</span>
					</label>


					<?php
}

		?>

				<div class="um-clear"></div>
				<div class="um-clear"></div>
			</div>
		</div>

		<?php

	}

}