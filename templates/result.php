<?php

if (isset($_GET['result'])) {

	echo '<div style="margin-bottom:15px">';

	if ($_GET['result'] == 'success') {
		echo '<p class="um-notice success">' . __('Your message was sent successfully.', 'pp-contact') . '</p>';
	}

	if ($_GET['result'] == 'fail') {
		echo '<p class="um-notice err">' . __('There was an error sending your message. Please contact the site administrator.', 'pp-contact') . '</p>';
	}

	if ($_GET['result'] == 'bot') {
		echo '<p class="um-notice err">' . __('Please verify that you are not a robot.', 'pp-contact') . '</p>';
	}

	echo '</div>';

}

?>