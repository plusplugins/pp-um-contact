<?php

$sitekey   = um_get_option( 'g_recaptcha_sitekey' );
$recaptcha = ( um_get_option( 'pp_contact_recaptcha' ) == 1 );
$uid       = isset( $user_id ) ? $user_id : um_profile_id();

$defaults = array(
	'name'  => '',
	'email' => '',
);

if ( is_user_logged_in() ) {
	$userdata          = get_userdata( get_current_user_id() );
	$defaults['name']  = $userdata->display_name;
	$defaults['email'] = $userdata->user_email;
}
?>
<form method="post" id="pp-um-contact" action="<?php echo admin_url( 'admin-ajax.php' ); ?>">
	<?php wp_nonce_field( 'pp-um-contact-form-submit', 'pp-um-contact-form' ); ?>
	<input type="hidden" name="uid" value="<?php echo $uid; ?>">
	<input type="hidden" name="action" value="pp_contact_submit">

	<div class="um-field um-field-text um-field-name">
		<div class="um-field-label">
			<label for="name"><?php _e( 'Name', 'pp-contact' ); ?></label>
			<div class="um-clear"></div>
		</div>
		<div class="um-field-area">
			<input class="um-form-field valid" type="text" name="name" id="name" value="<?php echo $defaults['name']; ?>" placeholder="<?php _e( 'Name', 'pp-contact' ); ?>">
		</div>
	</div>

	<div class="um-field um-field-text um-field-email">
		<div class="um-field-label">
			<label for="email"><?php _e( 'Email', 'pp-contact' ); ?></label>
			<div class="um-clear"></div>
		</div>
		<div class="um-field-area">
			<input class="um-form-field valid" type="text" name="email" id="email" value="<?php echo $defaults['email']; ?>" placeholder="<?php _e( 'Name', 'pp-contact' ); ?>">
		</div>
	</div>

	<div class="um-field um-field-textarea um-field-message">
		<div class="um-field-label">
			<label for="message"><?php _e( 'Message', 'pp-contact' ); ?></label>
			<div class="um-clear"></div>
		</div>
		<div class="um-field-area">
			<textarea style="height: 100px;" class="um-form-field valid " name="message" id="message" placeholder="<?php _e( 'Message', 'pp-contact' ); ?>"></textarea>
		</div>
	</div>
	<div class="um-clear"></div>

	<?php if ( $recaptcha && $sitekey ) { ?>
		<div class="g-recaptcha" style="margin-top:30px" data-sitekey="<?php echo $sitekey; ?>"></div>
	<?php } ?>

	<div class="um-col-alt" style="margin-top:15px;">
		<div class="um-left um-half">
			<input type="submit" value="<?php _e( 'Send', 'pp-contact' ); ?>" class="um-button">
		</div>
		<div class="um-clear"></div>
	</div>
</form>
<style>
	.um-field-label {
		border-bottom: none !important;
	}
</style>

<script type="text/javascript">
	jQuery("#pp-um-contact").on('submit', function (event) {
		jQuery(this).find('input[type="text"], textarea').each(function (index, element) {
			if (jQuery(element).val() == '') {
				event.preventDefault();
				jQuery(element).parent().parent().append('<div class="um-field-error"><span class="um-field-arrow"><i class="um-faicon-caret-up"></i></span>Required field.</div>').find('.um-field-error').delay(3000).fadeOut('slow');

			}
		});
	});
</script>
