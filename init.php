<?php

class PP_Contact {
	public $notice_messge = '';
	public $plugin_inactive = false;

	function __construct() {
		$this->plugin_inactive = false;

		add_action( 'init', array( $this, 'plugin_check' ), 10 );
		add_action( 'init', array( $this, 'init' ), 20 );
		add_action( 'admin_notices', array( $this, 'add_notice' ), 20 );
	}

	function plugin_check() {
		if ( ! class_exists( 'UM_API' ) ) {
			$this->notice_messge   = __( 'The <strong>Ultimate Member Contact Form</strong> extension requires the Ultimate Member plugin to be activated to work properly. You can download it <a href="https://wordpress.org/plugins/ultimate-member">here</a>', 'pp-contact' );
			$this->plugin_inactive = true;
		} else if ( ! version_compare( ultimatemember_version, PP_CONTACT_REQUIRES, '>=' ) ) {
			$this->notice_messge   = __( 'The <strong>Ultimate Member Contact Form</strong> extension requires a <a href="https://wordpress.org/plugins/ultimate-member">newer version</a> of Ultimate Member to work properly.', 'pp-contact' );
			$this->plugin_inactive = true;
		}
	}

	function add_notice() {
		if ( ! is_admin() || empty( $this->notice_messge ) ) {
			return;
		}

		echo '<div class="error"><p>' . $this->notice_messge . '</p></div>';
	}

	function init() {
		if ( $this->plugin_inactive ) {
			return;
		}

		require PP_CONTACT_PLUGIN_DIR . 'core.php';
		require PP_CONTACT_PLUGIN_DIR . 'settings.php';

		$this->core     = new PP_Contact_Core();
		$this->settings = new PP_Contact_Settings();
	}
}

$pp_contact = new PP_Contact();
