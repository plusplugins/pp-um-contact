jQuery(document).ready(function() {

    jQuery('.pp-contact-dir-btn').on('click', function(e) {

        e.preventDefault();
        prepare_Modal();

        var uid = jQuery(this).attr('data-uid');

        jQuery.ajax({
            url: um_scripts.ajaxurl,
            type: 'post',
            data: {
                action: 'pp_contact_show_modal',
                uid: uid,
            },
            
            success: function(data) {
                if (data) {
                    show_Modal(data);
                    responsive_Modal();
                    um_responsive();
                } else {
                    remove_Modal();
                }
            },

        });

        return false;
    });
});