<?php

class PP_Contact_Core {
	function __construct() {
		add_filter( 'um_profile_tabs', array( $this, 'add_profile_tab' ), 2000 );
		add_action( 'um_profile_content_contact_default', array( $this, 'show_profile_tab_content' ) );
		add_action( 'um_members_just_after_name', array( $this, 'directory_button' ), 110, 2 );
		add_action( 'wp_ajax_pp_contact_submit', array( $this, 'handle_form' ) );
		add_action( 'wp_ajax_nopriv_pp_contact_submit', array( $this, 'handle_form' ) );
		add_action( 'wp_ajax_pp_contact_show_modal', array( $this, 'show_modal' ) );
		add_action( 'wp_ajax_nopriv_pp_contact_show_modal', array( $this, 'show_modal' ) );
		add_action( 'um_members_directory_search', array( $this, 'show_result' ), 1 );
		add_action( 'um_account_pre_update_profile', array($this, 'pp_submit_account_details'));
	}

	function add_profile_tab( $tabs ) {
		if ( $this->should_display( um_profile_id() ) ) {
			$tabs['contact'] = array(
				'name'   => __( 'Contact', 'pp-contact' ),
				'icon'   => 'um-faicon-envelope',
				'custom' => true,
			);
		}

		return $tabs;
	}

	function show_profile_tab_content() {
		include PP_CONTACT_PLUGIN_DIR . 'templates/result.php';
		include PP_CONTACT_PLUGIN_DIR . 'templates/form.php';
	}

	function handle_form() {
		if ( isset( $_POST['pp-um-contact-form'] ) ) {
			if ( ! wp_verify_nonce( $_POST['pp-um-contact-form'], 'pp-um-contact-form-submit' ) ) {
				wp_die( __( "Security error", 'pp-contact' ) );
			}

			$recaptcha = ( um_get_option( 'pp_contact_recaptcha' ) == 1 );

			if ( $recaptcha ) {
				$secret                  = um_get_option( 'g_recaptcha_secretkey' );
				$client_captcha_response = $_POST['g-recaptcha-response'];
				$user_ip                 = $_SERVER['REMOTE_ADDR'];

				$response = wp_remote_get( "https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$client_captcha_response}&remoteip={$user_ip}" );

				if ( is_array( $response ) ) {
					$result = json_decode( $response['body'] );

					if ( ! $result->success ) {
						$url = add_query_arg( 'result', 'bot', $_SERVER['HTTP_REFERER'] );
						wp_redirect( $url );
						exit;
					}
				} else {
					$url = add_query_arg( 'result', 'bot', $_SERVER['HTTP_REFERER'] );
					wp_redirect( $url );
					exit;
				}
			}

			$args = array_map( 'sanitize_text_field', $_POST );
			$this->send_email( $args );
			$url = add_query_arg( 'result', 'success', $_SERVER['HTTP_REFERER'] );
			wp_redirect( $url );
			exit;
		}
	}

	function send_email( $args ) {
		global $ultimatemember;

		$name    = $args['name'];
		$email   = $args['email'];
		$message = $args['message'];
		$uid     = $args['uid'];

		$userdata   = get_userdata( $uid );
		$user_name  = $userdata->display_name;
		$user_email = $userdata->user_email;

		$ultimatemember->mail->send( $user_email, 'pp_contact_mail', array(
			'plain_text'   => 1,
			'tags'         => array(
				'{sitename}',
				'{recipient_name}',
				'{display_name}',
				'{sender_name}',
				'{sender_email}',
				'{message}',
			),
			'tags_replace' => array(
				get_bloginfo( 'name' ),
				$user_name,
				$user_name,
				$name,
				$email,
				$message,
			),
		) );

		return true;
	}

	function directory_button( $user_id, $args ) {
		if ( isset( $args['pp_contact_form_button'] ) ) {
			if ( $args['pp_contact_form_button'] == 'on' ) {
				if ( $user_id != get_current_user_id() ) {
					if ( $this->should_display( $user_id ) ) {
						wp_enqueue_script( 'pp-contact', PP_CONTACT_PLUGIN_URI . 'assets/js/pp-contact.js', array( 'jquery' ), false, true );
						wp_enqueue_style( 'pp-contact', PP_CONTACT_PLUGIN_URI . 'assets/css/pp-contact.css' );
						$url = add_query_arg( 'profiletab', 'contact', um_user_profile_url() );
						?>
						<div class="um-clear"></div>
						<a href="#" class="um-button pp-contact-dir-btn" data-uid="<?php echo $user_id; ?>">
							<?php _e( 'Contact', 'pp-contact' ); ?>
						</a>
						<?php
					}
				}
			}
		}
	}

	function should_display( $user_id ) {
		$profile_role = get_user_meta( $user_id, 'role', true );
		$user_role    = get_user_meta( get_current_user_id(), 'role', true );
		$have_roles   = um_get_option( 'pp_contact_have_roles' );
		$view_roles   = um_get_option( 'pp_contact_view_roles' );
		$hide         = ( get_user_meta( $user_id, 'pp_hide_contact_form', true ) == 'Yes' );

		if ( $hide ) {
			return false;
		}

		if ( ! is_user_logged_in() ) {
			$user_role = 'visitor';
		}

		if ( isset( $have_roles ) ) {
			if ( ! in_array( $profile_role, $have_roles ) ) {
				return false;
			}
		}

		if ( isset( $view_roles ) ) {
			if ( ! in_array( $user_role, $view_roles ) ) {
				return false;
			}
		}

		return true;
	}

	function show_modal() {
		$user_id = $_POST['uid'];
		ob_start();
		?>
		<div class="um-message-modal">
			<div class="um-message-header um-popup-header">
				<div class="um-message-header-left"><?php _e( 'Contact', 'pp-contact' ); ?></div>
				<div class="um-message-header-right">
					<a href="javascript:remove_Modal();" class="um-message-hide"><i class="um-icon-close"></i></a>
				</div>
			</div>
			<div class="um-message-body um-popup-autogrow2 um-message-autoheight">
				<div class="um" style="padding:0 10px;max-width: 470px;">
					<?php include PP_CONTACT_PLUGIN_DIR . 'templates/form.php' ?>
				</div>
			</div>
		</div>
		<?php
		$html = ob_get_contents();
		ob_end_clean();
		die( $html );
	}

	function show_result() {
		include PP_CONTACT_PLUGIN_DIR . 'templates/result.php';
	}

	/***
	*** @submit account page changes
	*** //fired on account page, just before updating profile
	*** @hooked to do_action('um_account_pre_update_profile', $changes, um_user('ID') );
	***/
	function pp_submit_account_details( $changes ) {
		global $ultimatemember;

		$changes['pp_hide_contact_form'] = $_POST['pp_hide_contact_form'];

		if ( isset( $changes['pp_hide_contact_form'] ) && $changes['pp_hide_contact_form'] == __('No') ) {
			delete_user_meta( um_user('ID'), 'pp_hide_contact_form' );
			unset( $changes['pp_hide_contact_form'] );
		} 
		if ( isset( $changes['pp_hide_contact_form'] ) && $changes['pp_hide_contact_form'] == __('Yes') ) {
			update_user_meta(um_user('ID'), 'pp_hide_contact_form', 'Yes');
		}
	}
}
